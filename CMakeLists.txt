cmake_minimum_required(VERSION 3.18)

project(HDF5Kit CXX)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)
set(CMAKE_CXX_EXTENSIONS OFF)

include(cmake/StandardProjectSettings.cmake)
include(cmake/PreventInSourceBuilds.cmake)
include(cmake/CompilerWarnings.cmake)
include(cmake/StaticAnalyzers.cmake)
include(cmake/Sanitizers.cmake)
include(cmake/Doxygen.cmake)
# To update CPM, execute the following in the project directory:
# wget -O cmake/CPM.cmake https://github.com/cpm-cmake/CPM.cmake/releases/latest/download/get_cpm.cmake
include(cmake/CPM.cmake)
set(WARNINGS_AS_ERRORS $ENV{WARNINGS_AS_ERRORS})

enable_doxygen()
enable_testing()

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
if (CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=lld")
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
else ()
    message(ERROR "Compiler '${CMAKE_CXX_COMPILER_ID}' not supported.")
endif ()
string(REPLACE ":" ";" LD_LIB_PATH_LIST "$ENV{LD_LIBRARY_PATH}")
foreach (LD_LIB_PATH ${LD_LIB_PATH_LIST})
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,-rpath,${LD_LIB_PATH}")
endforeach ()
set(KMIN_HDF5LIB "-lhdf5")

# UtilityKit
CPMAddPackage(
    NAME UtilityKit
    GIT_TAG 76e41beab464bd26f9703edeb1ef2f01343b678e
    GIT_REPOSITORY https://gitlab.com/kyungguk/UtilityKit.git
    DOWNLOAD_ONLY YES
)
add_subdirectory("${UtilityKit_SOURCE_DIR}/UtilityKit")
target_link_libraries(UtilityKit
    PUBLIC "-lpthread"
    )

# HDF5Kit
add_subdirectory(HDF5Kit)
target_link_libraries(HDF5Kit
    PUBLIC "${KMIN_HDF5LIB}"
    PUBLIC UtilityKit
    )
