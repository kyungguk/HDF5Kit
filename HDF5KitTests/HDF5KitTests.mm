//
//  HDF5KitTests.mm
//  HDF5KitTests
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#define HDF5KIT_INLINE_VERSION 1
#include <HDF5Kit/HDF5Kit.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <string>

@interface HDF5KitTests : XCTestCase

@end

@implementation HDF5KitTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPList {
    try {
        H5::PList plist;
        XCTAssert(!!plist);
        plist = H5::PList{H5P_FILE_ACCESS};
        plist = H5::PList(H5P_GROUP_CREATE);
        plist = H5::PList(H5P_DATASET_CREATE);
        plist.set_chunk({1024});
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testType {
    try {
        H5::Type type;
        XCTAssert(!type);
        type = static_cast<H5::Type const&>(H5::Type{});
        XCTAssert(!type);
        type = H5::Type::native<int>();
        XCTAssert(type);
        type = H5::Type{H5T_STRING, 10};
        XCTAssert(type);
        type = H5::Type::array<UTL::Vector<char, 3>>();
        XCTAssert(type);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testSpace {
    try {
        H5::Space space;
        try {
            space.rank();
            XCTAssert(false);
        } catch (std::exception &) {
        }
        XCTAssert(!space);
        space = static_cast<H5::Space const&>(H5::Space{});
        XCTAssert(!space);
        space = H5::Space{nullptr};
        XCTAssert(0 == space.rank());

        H5::Space::size_array_type dims, max_dims;
        space = H5::Space{dims};
        XCTAssert(!!space && 0 == space.rank());
        std::tie(dims, max_dims) = space.simple_extent();
        XCTAssert(dims.empty() && max_dims.empty());
        space = H5::Space{dims, max_dims};
        XCTAssert(!!space);
        std::tie(dims, max_dims) = space.simple_extent();
        XCTAssert(dims.empty() && max_dims.empty());

        dims = {2, 3};
        max_dims = dims;
        space = H5::Space{dims, max_dims};
        XCTAssert(!!space && dims.size() == space.rank());
        H5::Space::size_array_type start, count;
        std::tie(start, count) = space.simple_extent();
        XCTAssert(dims.size() == start.size() && dims.size() == count.size());
        for (long i = 0; i < dims.size(); ++i) {
            XCTAssert(dims[i] == start[i] && dims[i] == count[i]);
        }
        XCTAssert(space.is_simple());

        space.select_all();
        space.select_none();
        space.select(H5S_SELECT_SET, start, count);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testHDF5 {
    NSString *filepath = [NSHomeDirectory() stringByAppendingPathComponent:@"Downloads/test.h5"];
    UTL::DynamicArray<double> const d{1, 2, 3, 4, 5};
    UTL::Vector<double, 3> const v{1, 2, 3};

    // hdf5 create
    try {
        std::string s;
        // file
        //
        H5::File _file(H5::File::Trunc{}, filepath.UTF8String, H5::PList::fapl(), H5::PList::fcpl()), file(std::move(_file));
        s = file.file_path();
        XCTAssert(s == filepath.UTF8String, @"%s", s.c_str());
        s = file.path();
        XCTAssert(s == "/", @"%s", s.c_str());
        XCTAssert(!!file.cpl() && !!file.apl());
        file.flush();
        H5::Group &root = file;
        // group attribute
        //
        H5::Attribute gattr = root.attribute("attr", H5::Type::native<double>(), H5::Space{nullptr}, H5::PList{}, H5::PList::acpl());
        gattr.write(d.data());
        gattr.flush();
        s = gattr.name();
        XCTAssert(s == "attr" && 0 == gattr.space().rank());
        gattr = root.attribute("attr_v", H5::Type::array<decltype(v)>(), H5::Space{nullptr}, H5::PList{}, H5::PList::acpl());
        gattr.write(&v);
        XCTAssert(0 == gattr.space().rank());
        // subgroup
        //
        H5::Group group = root.group("group", H5::PList::gapl(), H5::PList::gcpl(), H5::PList::lcpl());
        s = group.path();
        XCTAssert(s == "/group");
        // dataset
        //
        H5::Dataset dset = group.dataset("dset", H5::Type::native<double>(), H5::Space{{static_cast<unsigned>(d.size())}}, H5::PList::dapl(), H5::PList::dcpl(), H5::PList::lcpl());
        H5::Space space = dset.space();
        space.select_all();
        dset.write(space, d.data(), space, H5::PList::dxpl());
        s = dset.path();
        XCTAssert(s == "/group/dset");
        // chunked dataset
        //
        H5::PList dcpl = H5::PList::dcpl();
        dcpl.set_chunk({64});
        H5::Dataset chunk = group.dataset("chunk", H5::Type::native<double>(), H5::Space{{0}, nullptr}, H5::PList::dapl(), dcpl);
        chunk.set_extent({static_cast<unsigned>(d.size())});
        space = chunk.space();
        space.select_all();
        chunk.write(space, d.data(), space);
        // scalar vector dataset
        //
        dset = group.dataset("vector", H5::Type::array<decltype(v)>(), H5::Space{nullptr});
        space = dset.space();
        space.select_all();
        dset.write(space, &v, space);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }

    // hdf5 open
    try {
        std::string s;
        // file
        //
        H5::File _file(H5::File::Rdonly{}, filepath.UTF8String, H5::PList::fapl()), file(std::move(_file));
        s = file.file_path();
        XCTAssert(s == filepath.UTF8String, @"%s", s.c_str());
        s = file.path();
        XCTAssert(s == "/", @"%s", s.c_str());
        H5::Group &root = file;
        // group attribute
        //
        H5::Attribute gattr = root.attribute("attr", H5::PList{});
        s = gattr.name();
        XCTAssert(s == "attr" && 0 == gattr.space().rank());
        if (gattr.space().rank() == 0) {
            double x;
            gattr.read(&x);
            XCTAssert(x == d[0], @"x = %f", x);
        }
        gattr = root.attribute("attr_v");
        XCTAssert(0 == gattr.space().rank());
        if (gattr.space().rank() == 0) {
            auto v2 = v;
            gattr.read(&v2);
            for (long i = 0; i < v.size(); ++i) {
                XCTAssert(v[i] == v2[i], @"v2[%ld] = %f", i, v2[i]);
            }
        }
        // subgroup
        //
        H5::Group group = root.group("group", H5::PList::gapl());
        s = group.path();
        XCTAssert(s == "/group");
        // dataset
        //
        H5::Dataset dset = group.dataset("dset", H5::PList::dapl());
        s = dset.path();
        XCTAssert(s == "/group/dset");
        H5::Space space = dset.space();
        auto dims = space.simple_extent().first;
        XCTAssert(dims.size() == 1 && dims[0] == static_cast<unsigned>(d.size()));
        UTL::DynamicArray<double> buf(d.size());
        space.select_all();
        dset.read(space, buf.data(), space, H5::PList::dxpl());
        for (long i = 0; i < buf.size(); ++i) {
            XCTAssert(d[i] == buf[i]);
        }
        // chunked dataset
        //
        H5::Dataset chunk = group.dataset("chunk");
        space = chunk.space();
        dims = space.simple_extent().first;
        XCTAssert(dims.size() == 1 && dims[0] == static_cast<unsigned>(d.size()));
        buf.resize(d.size());
        space.select_all();
        chunk.read(space, buf.data(), space, H5::PList::dxpl());
        for (long i = 0; i < buf.size(); ++i) {
            XCTAssert(d[i] == buf[i]);
        }
        // scalar vector dataset
        //
        dset = group.dataset("vector");
        (space = dset.space()).select_all();
        auto v2 = v;
        dset.read(space, &v2, space);
        for (long i = 0; i < v.size(); ++i) {
            XCTAssert(v[i] == v2[i], @"v2[%ld] = %f", i, v2[i]);
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
