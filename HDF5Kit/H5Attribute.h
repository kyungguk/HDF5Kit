//
//  H5Attribute.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Attribute_h
#define H5Attribute_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5Type.h>
#include <string>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Dataset;
    class Group;
    class Space;
    class PList;

    /**
     @brief Wrapper for HDF5 attribute.
     */
    class Attribute final : public Object {
        friend Dataset;
        friend Group;

        explicit Attribute(decltype(nullptr), hid_t const id);
    public:
        // move semantic
        Attribute(Attribute &&o) noexcept = default;
        Attribute &operator=(Attribute &&o) noexcept = default;

        /**
         @brief Construct Attribute object whose state is invalid.
         */
        explicit Attribute() noexcept : Object() {}

        // attribute attributes
        //
        /**
         @brief Name of *this.
         */
        std::string name() const;
        /**
         @brief Data space of *this.
         */
        Space space() const;
        /**
         @brief Data type of *this.
         */
        Type type() const;
        /**
         @brief Creation PList of *this.
         */
        PList cpl() const;

        // read/write operations
        //
        /**
         @brief Flush *this out to disk.
         @discussion Local file scope.
         */
        void flush();

        /**
         @brief Write out data of the given type.
         */
        void write(void const *data, Type const &type);
        /**
         @brief Write out data of one of the native types.
         */
        template <class T>
        void write(T const *data) { write(data, Type::native<T>()); }
        /**
         @brief Write out data of vector types.
         */
        template <class T, long S>
        void write(UTL::Vector<T, S> const *v) { write(v->data(), Type::array<UTL::Vector<T, S>>()); }

        /**
         @brief Read in data of the given type.
         */
        void read(void *data, Type const &type) const;
        /**
         @brief Read in data of one of the native types.
         */
        template <class T>
        void read(T *data) const { read(data, Type::native<T>()); }
        /**
         @brief Read in data of vector types.
         */
        template <class T, long S>
        void read(UTL::Vector<T, S> *v) const { read(v->data(), Type::array<UTL::Vector<T, S>>()); }

    private:
        // to be used by Groud and Dataset (so use hid_t)
        explicit Attribute(hid_t const &parent, char const *name, Type const &type, Space const &space, PList const &aapl, PList const &acpl); // creation
        explicit Attribute(hid_t const &parent, char const *name, PList const &aapl); // opening
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Attribute_h */
