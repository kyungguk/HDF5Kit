//
//  H5Dataset.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Dataset_h
#define H5Dataset_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>
#include <HDF5Kit/H5Type.h>
#include <utility>
#include <string>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Attribute;
    class Group;
    class Space;

    /**
     @brief Wrapper for HDF5 attribute.
     */
    class Dataset final : public Object {
        friend Group;

        std::string _path;
        explicit Dataset(decltype(nullptr), hid_t const id);
    public:
        // move semantic
        Dataset(Dataset &&o) = default; // noexcept is removed because of older version gcc error
        Dataset &operator=(Dataset &&o) = default;

        /**
         @brief Construct Dataset object whose state is invalid.
         */
        explicit Dataset() noexcept : Object(), _path() {}

        // dataset attributes
        //
        /**
         @brief Path of *this in HDF5 tree.
         */
        char const *path() const { return _path.c_str(); }
        /**
         @brief Data type of *this.
         */
        Type type() const;
        /**
         @brief Creation PList of *this.
         */
        PList cpl() const;
        /**
         @brief Access PList of *this.
         */
        PList apl() const;
        /**
         @brief Data space of *this.
         */
        Space space() const;
        /**
         @brief Set extent of *this.
         */
        void set_extent(size_array_type const &dims);

        // attribute access/creation
        //
        /**
         @brief Create a named attribute under *this.
         */
        Attribute attribute(char const *name, Type const &type, Space const &space, PList const &aapl = PList{}, PList const &acpl = PList{});
        /**
         @brief Open a named attribute under *this.
         */
        Attribute attribute(char const *name, PList const &aapl = PList{}) const;

        // read/write operations
        //
        /**
         @brief Flush *this out to disk.
         @discussion Local file scope.
         */
        void flush();

        /**
         @brief Write out data of the given type and dimensions.
         */
        void write(Space const &fspace, std::pair<void const *, Type const &> data, Space const &mspace, PList const &xfer = PList{});
        /**
         @brief Write out data of one of the native types.
         */
        template <class T>
        void write(Space const &fspace, T const *data, Space const &mspace, PList const &xfer = PList{}) { write(fspace, {data, Type::native<T>()}, mspace, xfer); }
        /**
         @brief Write out data of vector types.
         */
        template <class T, long S>
        void write(Space const &fspace, UTL::Vector<T, S> const *v, Space const &mspace, PList const &xfer = PList{}) { write(fspace, {v->data(), Type::array<UTL::Vector<T, S>>()}, mspace, xfer); }

        /**
         @brief Read in data of the given type and dimensions.
         */
        void read(Space const &fspace, std::pair<void *, Type const &> data, Space const &mspace, PList const &xfer = PList{}) const;
        /**
         @brief Read in data of one of the native types.
         */
        template <class T>
        void read(Space const &fspace, T *data, Space const &mspace, PList const &xfer = PList{}) const { read(fspace, {data, Type::native<T>()}, mspace, xfer); }
        /**
         @brief Read in data of vector types.
         */
        template <class T, long S>
        void read(Space const &fspace, UTL::Vector<T, S> *v, Space const &mspace, PList const &xfer = PList{}) const { read(fspace, {v->data(), Type::array<UTL::Vector<T, S>>()}, mspace, xfer); }

    private:
        // to be used by Groud and Dataset
        explicit Dataset(Group const &parent, char const *name, Type const &type, Space const &space, PList const &dapl, PList const &dcpl, PList const &lcpl); // creation
        explicit Dataset(Group const &parent, char const *name, PList const &dapl); // opening
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Dataset_h */
