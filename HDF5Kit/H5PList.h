//
//  H5PList.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5PList_h
#define H5PList_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Attribute;
    class Dataset;
    class Group;
    class File;
    class Type;

    /**
     @brief Wrapper for HDF5 property list.
     */
    class PList final : public Object {
        friend Attribute;
        friend Dataset;
        friend Group;
        friend File;
        friend Type;

        explicit PList(decltype(nullptr), hid_t const id);
    public:
        // move/copy semantic
        PList(PList &&o) noexcept = default;
        PList &operator=(PList &&o) noexcept = default;
        PList(PList const &o);
        PList &operator=(PList const &o) { return *this = PList{o}; }

        /**
         @brief Construct default PList object.
         @discussion Wrapper for H5P_DEFAULT.
         */
        explicit PList() noexcept : Object(H5P_DEFAULT, nullptr) {}
        /**
         @brief Construct PList object of the given class.
         */
        explicit PList(hid_t const plist_class);

        // Datatype property list operations
        //
        /// Create datatype creation PList.
        static PList tcpl() { return PList{H5P_DATATYPE_CREATE}; }
        /// Create datatype access PList.
        static PList tapl() { return PList{H5P_DATATYPE_ACCESS}; }

        // Attribute property list operations
        //
        /// Create attribute creation PList.
        static PList acpl() { return PList{H5P_ATTRIBUTE_CREATE}; }

        // Group property list operations
        //
        /// Create group creation PList.
        static PList gcpl() { return PList{H5P_GROUP_CREATE}; }
        //
        /// Create group access PList.
        static PList gapl() { return PList{H5P_GROUP_ACCESS}; }

        // File property list operations
        //
        /// Create file creation PList.
        static PList fcpl() { return PList{H5P_FILE_CREATE}; }
        //
        /// Create file access PList.
        static PList fapl() { return PList{H5P_FILE_ACCESS}; }

        // Link property list operations
        //
        /// Create link creation PList.
        static PList lcpl() { return PList{H5P_LINK_CREATE}; }
        //
        /// Create link access PList.
        static PList lapl() { return PList{H5P_LINK_ACCESS}; }

        // Dataset property list operations
        //
        /**
         @brief Set chunk sizes of dataset creation PList.
         */
        void set_chunk(size_array_type const &dims);
        /// Create dataset creation PList.
        static PList dcpl() { return PList{H5P_DATASET_CREATE}; }
        /// Create dataset access PList.
        static PList dapl() { return PList{H5P_DATASET_ACCESS}; }
        /// Create dataset transfer PList.
        static PList dxpl() { return PList{H5P_DATASET_XFER}; }
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5PList_h */
