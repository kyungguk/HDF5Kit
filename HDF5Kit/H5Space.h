//
//  H5Space.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Space_h
#define H5Space_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>
#include <utility>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Attribute;
    class Dataset;

    /**
     @brief Wrapper for HDF5 data space.
     */
    class Space final : public Object {
        friend Attribute;
        friend Dataset;

        explicit Space(decltype(nullptr), hid_t const id);
    public:
        // move/copy semantic
        Space(Space &&o) noexcept = default;
        Space &operator=(Space &&o) noexcept = default;
        Space(Space const &o);
        Space &operator=(Space const &o) { return *this = Space{o}; }

        /**
         @brief Construct Space object whose state is invalid.
         */
        explicit Space() noexcept : Object() {}
        /**
         @brief Construct Space object of the given class.
         */
        explicit Space(H5S_class_t const space_class);
        /**
         @brief Construct simple Space object of the given dimension sizes with the maximum dimension sizes.
         */
        explicit Space(size_array_type const &dims, size_array_type const &max_dims);
        /**
         @brief Construct simple Space object of the given dimension sizes.
         @discussion The upper limit is the same as the dimension sizes.
         */
        explicit Space(size_array_type const &dims) : Space(dims, dims) {}
        /**
         @brief Construct simple Space object of the given dimension sizes with an unbound maximum dimension sizes.
         @note The meaning of nullptr is different from the HDF5 library's H5Ssimple_create where nullptr indicates the upper limit the same as the dimension sizes.
         */
        explicit Space(size_array_type const &dims, decltype(nullptr)) : Space(dims, size_array_type(dims.size(), H5S_UNLIMITED)) {}
        /**
         @brief Construct scalar Space object.
         */
        explicit Space(decltype(nullptr)) : Space(H5S_SCALAR) {}

        /**
         @brief Determines whether a dataspace is a simple dataspace.
         */
        bool is_simple() const;
        /**
         @brief Get rank of simple space.
         */
        long rank() const;
        /**
         @brief Get simple extent.
         @return A pair of dimension sizes and maximum dimension sizes.
         */
        std::pair<size_array_type, size_array_type> simple_extent() const;

        // select operations
        //
        /**
         @brief Select all.
         */
        void select_all();
        /**
         @brief Select none.
         */
        void select_none();
        /**
         @brief Select hyperslab space.
         */
        void select(H5S_seloper_t const sel_op, size_array_type const &start, size_array_type const &count, size_array_type const &stride = size_array_type{}, size_array_type const &block = size_array_type{});
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Space_h */
