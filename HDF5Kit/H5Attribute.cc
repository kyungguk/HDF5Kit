//
//  H5Attribute.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Attribute.h"

// MARK: Version 1
//
#include <HDF5Kit/H5PList.h>
#include <HDF5Kit/H5Space.h>
#include <cstddef>
#include <stdexcept>
#include <memory>

H5::__1_::Attribute::Attribute(decltype(nullptr), hid_t const id)
: Object(id, &H5Aclose) {
}

H5::__1_::Attribute::Attribute(hid_t const &parent, char const *name, Type const &type, Space const &space, PList const &aapl, PList const &acpl)
: Attribute(nullptr, H5Acreate2(parent, name, *type, *space, *acpl, *aapl)) {
}
H5::__1_::Attribute::Attribute(hid_t const &parent, char const *name, PList const &aapl)
: Attribute(nullptr, H5Aopen(parent, name, *aapl)) {
}

std::string H5::__1_::Attribute::name() const
{
    auto const len = H5Aget_name(**this, 0, nullptr);
    if (len < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aget_name returned error");
    }
    std::unique_ptr<char[]> name{new char[static_cast<std::size_t>(len) + 1U]};
    if (H5Aget_name(**this, static_cast<unsigned long>(len) + 1U, name.get()) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aget_name returned error");
    }
    return name.get();
}
H5::__1_::Space H5::__1_::Attribute::space() const
{
    hid_t id = H5Aget_space(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aget_space returned error");
    }
    return Space{nullptr, id};
}
H5::__1_::Type H5::__1_::Attribute::type() const
{
    hid_t id = H5Aget_type(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aget_type returned error");
    }
    return Type{nullptr, id};
}
H5::__1_::PList H5::__1_::Attribute::cpl() const
{
    hid_t id = H5Aget_create_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aget_create_plist returned error");
    }
    return PList{nullptr, id};
}

void H5::__1_::Attribute::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Fflush returned error");
    }
}
void H5::__1_::Attribute::write(void const *data, Type const &type)
{
    if (H5Awrite(**this, *type, data) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Awrite returned error");
    }
}
void H5::__1_::Attribute::read(void *data, Type const &type) const
{
    if (H5Aread(**this, *type, data) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Aread returned error");
    }
}
