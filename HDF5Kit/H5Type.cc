//
//  H5Type.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Type.h"

// MARK: Version 1
//
#include <HDF5Kit/H5PList.h>
#include <stdexcept>
#include <string>

H5::__1_::Type::Type(decltype(nullptr), hid_t const id)
: Object(id, &H5Tclose) {
}
H5::__1_::Type::Type(Type const &o)
: Type() {
    if (o) { // protect against copying invalid object
        Type{nullptr, H5Tcopy(*o)}.swap(*this);
    }
}

H5::__1_::Type::Type(H5T_class_t const type_class, unsigned const sz)
: Type(nullptr, H5Tcreate(type_class, sz)) {
}
H5::__1_::Type::Type(Type const &base, size_array_type const &dims)
: Type() {
    if (dims.empty() || dims.size() > H5S_MAX_RANK) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid rank for array datatype");
    }
    for (auto const &x : dims) {
        if (0 == x) {
            throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - null dimension size for array datatype");
        }
    }
    *this = Type(nullptr, H5Tarray_create2(*base, static_cast<unsigned>(dims.size()), dims.data()));
}

H5::__1_::PList H5::__1_::Type::cpl() const
{
    hid_t id = H5Tget_create_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Tget_create_plist returned error");
    }
    return PList{nullptr, id};
}
