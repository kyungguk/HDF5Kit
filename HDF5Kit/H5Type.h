//
//  H5Type.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Type_h
#define H5Type_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>
#include <type_traits>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Attribute;
    class Dataset;
    class PList;

    /**
     @brief Wrapper for HDF5 data type.
     */
    class Type final : public Object {
        friend Attribute;
        friend Dataset;

        explicit Type(decltype(nullptr), hid_t const id);
    public:
        // move/copy semantic
        Type(Type &&o) noexcept = default;
        Type &operator=(Type &&o) noexcept = default;
        Type(Type const &o);
        Type &operator=(Type const &o) { return *this = Type{o}; }

        /**
         @brief Construct Type object whose state is invalid.
         */
        explicit Type() noexcept : Object() {}
        /**
         @brief Construct Type object of the given class.
         */
        explicit Type(H5T_class_t const type_class, unsigned const sz);
        /**
         @brief Construct array Type object of the given dimenions.
         @discussion The size of dims (i.e., rank of the array) must be greater than 0 and less than or equal to H5S_MAX_RANK, and the value of each element must be greater than 0.
         */
        explicit Type(Type const &base, size_array_type const &dims);

        // type attributes
        //
        /**
         @brief Creation PList of *this.
         */
        PList cpl() const;

        // native types
        //
        /// native char type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<char, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_CHAR, nullptr}; }
        /// native unsigned char type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<unsigned char, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_UCHAR, nullptr}; }

        /// native short type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<short, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_SHORT, nullptr}; }
        /// native unsigned short type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<unsigned short, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_USHORT, nullptr}; }

        /// native int type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<int, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_INT, nullptr}; }
        /// native unsigned int type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<unsigned int, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_UINT, nullptr}; }

        /// native long type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<long, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_LONG, nullptr}; }
        /// native unsigned long type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<unsigned long, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_ULONG, nullptr}; }

        /// native float type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<float, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_FLOAT, nullptr}; }
        /// native double type.
        template <class T>
        static auto native() -> typename std::enable_if<std::is_same<double, typename std::decay<T>::type>::value, Type>::type const { return Type{H5T_NATIVE_DOUBLE, nullptr}; }

        /// fixed-size array types.
        template <class A>
        static Type array() { return _array(static_cast<typename std::decay<A>::type const *>(nullptr)); }

    private:
        using Object::Object; // for native type construction

        // vector type helper
        //
        template <class T, long S>
        static auto _array(UTL::Vector<T, S> const *) -> typename std::enable_if<std::is_arithmetic<T>::value, Type>::type { return Type(native<T>(), {S}); }
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Type_h */
