//
//  H5Dataset.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Dataset.h"

// MARK: Version 1
//
#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Group.h>
#include <HDF5Kit/H5Space.h>
#include <stdexcept>

H5::__1_::Dataset::Dataset(decltype(nullptr), hid_t const id)
: Object(id, &H5Dclose), _path() {
}

H5::__1_::Dataset::Dataset(Group const &parent, char const *name, Type const &type, Space const &space, PList const &dapl, PList const &dcpl, PList const &lcpl)
: Dataset(nullptr, H5Dcreate2(*parent, name, *type, *space, *lcpl, *dcpl, *dapl)) {
    _path = parent.path();
    if ('/' == _path.at(_path.size() - 1)) {
        _path.pop_back();
    }
    _path = _path + "/" + name;
}
H5::__1_::Dataset::Dataset(Group const &parent, char const *name, PList const &dapl)
: Dataset(nullptr, H5Dopen2(*parent, name, *dapl)) {
    _path = parent.path();
    if ('/' == _path.at(_path.size() - 1)) {
        _path.pop_back();
    }
    _path = _path + "/" + name;
}

H5::__1_::Type H5::__1_::Dataset::type() const
{
    hid_t id = H5Dget_type(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dget_type returned error");
    }
    return Type{nullptr, id};
}
H5::__1_::PList H5::__1_::Dataset::cpl() const
{
    hid_t id = H5Dget_create_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dget_create_plist returned error");
    }
    return PList{nullptr, id};
}
H5::__1_::PList H5::__1_::Dataset::apl() const
{
    hid_t id = H5Dget_access_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dget_access_plist returned error");
    }
    return PList{nullptr, id};
}
H5::__1_::Space H5::__1_::Dataset::space() const
{
    hid_t id = H5Dget_space(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dget_space returned error");
    }
    return Space{nullptr, id};
}
void H5::__1_::Dataset::set_extent(size_array_type const &dims)
{
    if (dims.size() < space().rank()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - array size less than dimension rank");
    }
    if (H5Dset_extent(**this, dims.data()) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dset_extent returned error");
    }
}

H5::__1_::Attribute H5::__1_::Dataset::attribute(char const *name, Type const &type, Space const &space, PList const &aapl, PList const &acpl)
try {
    return Attribute{**this, name, type, space, aapl, acpl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
H5::__1_::Attribute H5::__1_::Dataset::attribute(char const *name, PList const &aapl) const
try {
    return Attribute{**this, name, aapl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}

void H5::__1_::Dataset::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Fflush returned error");
    }
}

void H5::__1_::Dataset::write(Space const &fspace, std::pair<void const *, Type const &> data, Space const &mspace, PList const &xfer)
{
    if (H5Dwrite(**this, *data.second, *mspace, *fspace, *xfer, data.first) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dwrite returned error");
    }
}
void H5::__1_::Dataset::read(Space const &fspace, std::pair<void *, Type const &> data, Space const &mspace, PList const &xfer) const
{
    if (H5Dread(**this, *data.second, *mspace, *fspace, *xfer, data.first) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Dread returned error");
    }
}

