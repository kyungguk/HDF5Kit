//
//  H5Group.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Group.h"

// MARK: Version 1
//
#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Dataset.h>
#include <HDF5Kit/H5Group.h>
#include <stdexcept>

H5::__1_::Group::Group(decltype(nullptr), hid_t const id)
: Object(id, &H5Gclose), _path() {
}

H5::__1_::Group::Group(Group const &parent, char const *name, PList const &gapl, PList const &gcpl, PList const &lcpl)
: Group(nullptr, H5Gcreate2(*parent, name, *lcpl, *gcpl, *gapl)) {
    _path = parent.path();
    if ('/' == _path.at(_path.size() - 1)) {
        _path.pop_back();
    }
    _path = _path + "/" + name;
}
H5::__1_::Group::Group(Group const &parent, char const *name, PList const &gapl)
: Group(nullptr, H5Gopen2(*parent, name, *gapl)) {
    _path = parent.path();
    if ('/' == _path.at(_path.size() - 1)) {
        _path.pop_back();
    }
    _path = _path + "/" + name;
}

H5::__1_::PList H5::__1_::Group::cpl() const
{
    hid_t id = H5Gget_create_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Gget_create_plist returned error");
    }
    return PList{nullptr, id};
}

void H5::__1_::Group::flush()
{
    if (H5Fflush(**this, H5F_SCOPE_LOCAL) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Fflush returned error");
    }
}

H5::__1_::Group H5::__1_::Group::group(char const *name, PList const &gapl, PList const &gcpl, PList const &lcpl)
try {
    return Group{*this, name, gapl, gcpl, lcpl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
H5::__1_::Group H5::__1_::Group::group(char const *name, PList const &gapl) const
try {
    return Group{*this, name, gapl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}

H5::__1_::Attribute H5::__1_::Group::attribute(char const *name, Type const &type, Space const &space, PList const &aapl, PList const &acpl)
try {
    return Attribute{**this, name, type, space, aapl, acpl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
H5::__1_::Attribute H5::__1_::Group::attribute(char const *name, PList const &aapl) const
try {
    return Attribute{**this, name, aapl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}

H5::__1_::Dataset H5::__1_::Group::dataset(char const *name, Type const &type, Space const &space, PList const &dapl, PList const &dcpl, PList const &lcpl)
try {
    return Dataset{*this, name, type, space, dapl, dcpl, lcpl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
H5::__1_::Dataset H5::__1_::Group::dataset(char const *name, PList const &dapl) const
try {
    return Dataset{*this, name, dapl};
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
