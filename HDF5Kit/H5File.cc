//
//  H5File.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5File.h"

// MARK: Version 1
//
#include <HDF5Kit/H5PList.h>
#include <cstddef>
#include <stdexcept>
#include <memory>

unsigned const H5::__1_::File::Trunc::value{H5F_ACC_TRUNC};
unsigned const H5::__1_::File::Excl::value{H5F_ACC_EXCL};
unsigned const H5::__1_::File::Rdwr::value{H5F_ACC_RDWR};
unsigned const H5::__1_::File::Rdonly::value{H5F_ACC_RDONLY};

H5::__1_::File::File(_Create, char const *filepath, unsigned const flags, PList const &fapl, PList const &fcpl)
try : Group(H5Fcreate(filepath, flags, *fcpl, *fapl), &H5Fclose, "/") {
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}
H5::__1_::File::File(_Open, char const *filepath, unsigned const flags, PList const &fapl)
try : Group(H5Fopen(filepath, flags, *fapl), &H5Fclose, "/") {
} catch (std::exception &e) {
    throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - " + e.what());
}

std::string H5::__1_::File::file_path() const
{
    // determine length
    //
    long const len = H5Fget_name(**this, nullptr, 0);
    if (len < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - failed to determine file length");
    }
    std::unique_ptr<char[]> path(new char[static_cast<std::size_t>(len) + 1U]);
    // retrieve file path
    //
    if (H5Fget_name(**this, path.get(), static_cast<unsigned long>(len) + 1U) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - failed to retrieve file path");
    }
    return path.get();
}
H5::__1_::PList H5::__1_::File::cpl() const
{
    hid_t id = H5Fget_create_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Fget_create_plist returned error");
    }
    return PList{nullptr, id};
}
H5::__1_::PList H5::__1_::File::apl() const
{
    hid_t id = H5Fget_access_plist(**this);
    if (id < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Fget_access_plist returned error");
    }
    return PList{nullptr, id};
}

