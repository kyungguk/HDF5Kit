//
//  H5PList.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5PList.h"

// MARK: Version 1
//
#include <stdexcept>
#include <string>

H5::__1_::PList::PList(decltype(nullptr), hid_t const id)
: Object(id, &H5Pclose) {
}
H5::__1_::PList::PList(PList const &o)
: PList() {
    if (o) { // protect against copying invalid object
        PList{nullptr, H5Pcopy(*o)}.swap(*this);
    }
}

H5::__1_::PList::PList(hid_t const plist_class)
: PList(nullptr, H5Pcreate(plist_class)) {
}

void H5::__1_::PList::set_chunk(const size_array_type &dims)
{
    if (H5Pset_chunk(**this, int(dims.size()), dims.data()) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Pset_chunk returned error");
    }
}
