//
//  H5File.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5File_h
#define H5File_h

// MARK:- Version 1
//
#include <HDF5Kit/H5Group.h>
#include <type_traits>
#include <string>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class PList;

    /**
     @brief Wrapper for HDF5 file.
     */
    class File final : public Group {
        // base class of file creation
        struct _Create {};
        // base class of file opening
        struct _Open {};

    public:
        // move semantic
        File(File &&o) = default; // noexcept is removed because of older version gcc error
        File &operator=(File &&o) = default; // noexcept is removed because of older version gcc error

        // file access flags
        //
        /**
         @brief Truncate file, if it already exists, erasing all data previously stored in the file.
         @discussion Only used in file creation.
         */
        struct Trunc : public _Create { static unsigned const value; };
        /**
         @brief Fail if file already exists.
         @discussion Only used in file creation.
         */
        struct Excl : public _Create { static unsigned const value; };
        /**
         @brief Allow read and write access to file.
         @discussion Only used in file opening.
         */
        struct Rdwr : public _Open { static unsigned const value; };
        /**
         @brief Allow read-only access to file.
         @discussion Only used in file opening.
         */
        struct Rdonly : public _Open { static unsigned const value; };

        /**
         @brief Construct File object whose state is invalid.
         */
        explicit File() noexcept : Group() {}
        /**
         @brief Construct File object by creating HDF5 file at filepath.
         */
        template <class AccessFlag, typename std::enable_if<std::is_base_of<_Create, AccessFlag>::value, long>::type = 0L>
        explicit File(AccessFlag flag, char const *filepath, PList const &fapl = PList{}, PList const &fcpl = PList{}) : File(flag, filepath, flag.value, fapl, fcpl) {}
        /**
         @brief Construct File object by opening HDF5 file at filepath.
         */
        template <class AccessFlag, typename std::enable_if<std::is_base_of<_Open, AccessFlag>::value, long>::type = 0L>
        explicit File(AccessFlag flag, char const *filepath, PList const &fapl = PList{}) : File(flag, filepath, flag.value, fapl) {}

        // file attributes
        //
        /**
         @brief Full path of HDF5 file representation in the filesystem.
         */
        std::string file_path() const;
        /**
         @brief Creation PList of *this.
         */
        PList cpl() const;
        /**
         @brief Access PList of *this.
         */
        PList apl() const;

    private:
        // actual creation/opening interface
        explicit File(_Create, char const *filepath, unsigned const flags, PList const &fapl, PList const &fcpl); // creation
        explicit File(_Open, char const *filepath, unsigned const flags, PList const &fapl); // opening
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5File_h */
