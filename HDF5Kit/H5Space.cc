//
//  H5Space.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Space.h"

// MARK: Version 1
//
#include <stdexcept>
#include <string>

H5::__1_::Space::Space(decltype(nullptr), hid_t const id)
: Object(id, &H5Sclose) {
}
H5::__1_::Space::Space(Space const &o)
: Space() {
    if (o) { // protect against copying invalid object
        Space{nullptr, H5Scopy(*o)}.swap(*this);
    }
}

H5::__1_::Space::Space(H5S_class_t const space_class)
: Space(nullptr, H5Screate(space_class)) {
}
H5::__1_::Space::Space(size_array_type const &dims, size_array_type const &max_dims)
: Space(nullptr, H5Screate_simple(int(dims.size()), dims.data(), max_dims.data())) {
}

bool H5::__1_::Space::is_simple() const
{
    htri_t const simple = H5Sis_simple(**this);
    if (simple < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sis_simple returned error");
    }
    return simple;
}
long H5::__1_::Space::rank() const
{
    int const rank = H5Sget_simple_extent_ndims(**this);
    if (rank < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sget_simple_extent_ndims returned error");
    }
    return rank;
}
auto H5::__1_::Space::simple_extent() const
-> std::pair<size_array_type, size_array_type> {
    decltype(simple_extent()) dims; {
        dims.first.resize(H5S_MAX_RANK);
        dims.second.resize(H5S_MAX_RANK);
    }
    int rank = H5Sget_simple_extent_dims(**this, dims.first.data(), dims.second.data());
    if (rank < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sget_simple_extent_dims returned error");
    } else {
        dims.first.resize(rank);
        dims.second.resize(rank);
    }
    return dims;
}

void H5::__1_::Space::select_all()
{
    if (H5Sselect_all(**this) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sselect_all returned error");
    }
}
void H5::__1_::Space::select_none()
{
    if (H5Sselect_none(**this) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sselect_none returned error");
    }
}
void H5::__1_::Space::select(H5S_seloper_t const sel_op, size_array_type const &start, size_array_type const &count, size_array_type const &stride, size_array_type const &block)
{
    // verify array sizes
    //
    long const rank = this->rank();
    if (start.size() < rank ||
        count.size() < rank ||
        (!stride.empty() && stride.size() < rank) ||
        (!block.empty() && block.size() < rank)) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - one or more of selection array sizes less than dimension rank");
    }

    // select
    //
    if (H5Sselect_hyperslab(**this, sel_op, start.data(), stride.empty() ? nullptr : stride.data(), count.data(), block.empty() ? nullptr : block.data()) < 0) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - H5Sselect_hyperslab returned error");
    }
}
