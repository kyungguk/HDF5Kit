//
//  HDF5Kit.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for HDF5Kit.
FOUNDATION_EXPORT double HDF5KitVersionNumber;

//! Project version string for HDF5Kit.
FOUNDATION_EXPORT const unsigned char HDF5KitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like #import <HDF5Kit/PublicHeader.h>


#if defined(__cplusplus)

#if !defined(HDF5KIT_INLINE_VERSION)
#define HDF5KIT_INLINE_VERSION 1
#endif

#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>
#include <HDF5Kit/H5Type.h>
#include <HDF5Kit/H5Space.h>
#include <HDF5Kit/H5Attribute.h>
#include <HDF5Kit/H5Dataset.h>
#include <HDF5Kit/H5Group.h>
#include <HDF5Kit/H5File.h>

#endif
