//
//  H5Group.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Group_h
#define H5Group_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <HDF5Kit/H5Object.h>
#include <HDF5Kit/H5PList.h>
#include <string>

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Attribute;
    class Dataset;
    class Space;
    class Type;

    /**
     @brief Wrapper for HDF5 attribute.
     */
    class Group : public Object {
        std::string _path;

        explicit Group(decltype(nullptr), hid_t const id);
    public:
        // move semantic
        Group(Group &&o) = default; // noexcept is removed because of older version gcc error
        Group &operator=(Group &&o) = default;

        /**
         @brief Construct Group object whose state is invalid.
         */
        explicit Group() noexcept : Object(), _path() {}

        // group attributes
        //
        /**
         @brief Path of *this in HDF5 tree.
         */
        char const *path() const noexcept(noexcept(_path.c_str())) { return _path.c_str(); }
        /**
         @brief Creation PList of *this.
         */
        PList cpl() const;

        /**
         @brief Flush *this out to disk.
         @discussion Local file scope.
         */
        void flush();

        // subgroup access/creation
        //
        /**
         @brief Create a named group under *this.
         */
        Group group(char const *name, PList const &gapl, PList const &gcpl, PList const &lcpl = PList{});
        /**
         @brief Open a named group under *this.
         */
        Group group(char const *name, PList const &gapl = PList{}) const;

        // attribute access/creation
        //
        /**
         @brief Create a named attribute under *this.
         */
        Attribute attribute(char const *name, Type const &type, Space const &space, PList const &aapl = PList{}, PList const &acpl = PList{});
        /**
         @brief Open a named attribute under *this.
         */
        Attribute attribute(char const *name, PList const &aapl = PList{}) const;

        // dataset access/creation
        //
        /**
         @brief Create a named dataset under *this.
         */
        Dataset dataset(char const *name, Type const &type, Space const &space, PList const &dapl = PList{}, PList const &dcpl = PList{}, PList const &lcpl = PList{});
        /**
         @brief Open a named dataset under *this.
         */
        Dataset dataset(char const *name, PList const &dapl = PList{}) const;

    private:
        // to be used by File
        explicit Group(Group const &parent, char const *name, PList const &gapl, PList const &gcpl, PList const &lcpl); // creation
        explicit Group(Group const &parent, char const *name, PList const &gapl); // opening

    protected:
        // for File construction
        explicit Group(hid_t const id, deleter_type deleter, std::string const& path) : Object(id, deleter), _path(path) {}
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Group_h */
