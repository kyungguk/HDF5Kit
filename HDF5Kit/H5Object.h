//
//  H5Object.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef H5Object_h
#define H5Object_h

#include <HDF5Kit/HDF5Kit-config.h>

// MARK:- Version 1
//
#include <UtilityKit/UtilityKit.h>
#include <utility>
#if defined(__APPLE__)
#include <hdf5/hdf5.h>
#else
#include <hdf5.h>
#endif

HDF5KIT_BEGIN_NAMESPACE
#if defined(HDF5KIT_INLINE_VERSION) && HDF5KIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    /**
     @brief Wrapper for HDF5 generic object.
     */
    class Object {
        Object(Object const&) = delete;
        Object &operator=(Object const&) = delete;

    public:
        // types
        //
        typedef herr_t (*deleter_type)(hid_t);
        using size_array_type = UTL::StaticArray<hsize_t, H5S_MAX_RANK>;

        /**
         @brief Native hid accessor.
         @exception When *this is invalid.
         */
        hid_t const &operator*() const;

        /**
         @brief Check whether *this is valid.
         */
        explicit operator bool() const noexcept { return _id >= 0; }

    protected: // for subclasses
        ~Object();
        explicit Object() noexcept : _id(-1), _deleter(nullptr) {}
        explicit Object(hid_t const raw, deleter_type const deleter); // exception when raw < 0
        Object(Object &&o) noexcept : Object() { o.swap(*this); }
        Object &operator=(Object &&o) noexcept { if (this != &o) Object{std::move(o)}.swap(*this); return *this; }

        void swap(Object &o) noexcept { std::swap(_id, o._id); std::swap(_deleter, o._deleter); }

    private:
        hid_t _id;
        deleter_type _deleter;
    };
} // namespace __1_
HDF5KIT_END_NAMESPACE

#endif /* H5Object_h */
