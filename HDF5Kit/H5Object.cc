//
//  H5Object.cc
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "H5Object.h"

// MARK: Version 1
//
#include <UtilityKit/UtilityKit.h>
#include <stdexcept>
#include <typeinfo>
#include <iostream>
#include <string>

H5::__1_::Object::~Object()
try {
    if (_deleter && _deleter(_id) < 0) {
        throw std::runtime_error(std::string(typeid(*this).name()) + "::" + __PRETTY_FUNCTION__ + " - failed to close h5 object");
    }
} catch (std::exception &e) {
    printo(std::cerr, e.what(), ".\n");
    //std::terminate();
}
H5::__1_::Object::Object(hid_t const raw, deleter_type const deleter)
: Object() {
    if (raw < 0) {
        throw std::invalid_argument(std::string(typeid(*this).name()) + "::" + __PRETTY_FUNCTION__ + " - invalid hid handle");
    }
    _id = raw;
    _deleter = deleter;
}

hid_t const &H5::__1_::Object::operator*() const
{
    if (!*this) {
        throw std::invalid_argument(std::string(typeid(*this).name()) + "::" + __PRETTY_FUNCTION__ + " - invalid hid handle");
    }
    return _id;
}
