//
//  HDF5Kit-config.h
//  HDF5Kit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef HDF5Kit_config_h
#define HDF5Kit_config_h


// root namespace
//
#ifndef HDF5KIT_NAMESPACE
#define HDF5KIT_NAMESPACE H5
#define HDF5KIT_BEGIN_NAMESPACE namespace HDF5KIT_NAMESPACE {
#define HDF5KIT_END_NAMESPACE }
#endif


#endif /* HDF5Kit_config_h */
